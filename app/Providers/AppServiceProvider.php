<?php

namespace App\Providers;

use App\Contact;
use App\Service;
use App\Treatment;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(config('app.env') === 'production') {
            URL::forceScheme('https');
        }
        $fallback_locale = 'en';
        $localLang = App::getLocale();
        View::share('service', Service::orderBy('id')->where("slugdisplay","!=","exapmle")->get()->translate($localLang, $fallback_locale));
        View::share('treatments', Treatment::orderBy('id')->where("slugdisplay","!=","exapmle")->get()->translate($localLang, $fallback_locale));
    }
}
