<?php

namespace App\Http\Controllers;

use App\Mail\ReviewNo;
use App\Mail\ReviewRequest;
use App\Negreview;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RequestController extends Controller
{
    public function send(Request $request){
        $request->validate([
            'name' => 'required|max:255',
            'mail' => 'required|max:255|email',
        ]);
        $name = $request->input('name');
        $email = $request->input('mail');
        try {
            $feedback = [
                'name' => $name,
                'mail' => $email,
            ];
            Mail::to($email)->send(new ReviewRequest($feedback));
        } catch (Exception $e) {
            return back()->with('swaled_status', json_encode([
                'title' => 'There was an error sending your message',
                'text' => ''.$e,
                'icon' => 'error',
            ]));
        }

        return back()->with('swaled_status', json_encode([
            'title' => 'Message sent to '.$email,
            'text' => $name.' has successfully received your mail',
            'icon' => 'success',
        ]));
    }
    public function no(Request $req){
        $message = $req->input('message');
        $mail = $req->input('mail');
        if($message==null){
            return response('Please fill the message field before sending the feedback to us.', 200)
                  ->header('Content-Type', 'text/plain');
        }
        try {
            $feed = [
                'message' => $message,
                'mail' => $mail,
            ];

            $attempt = new Negreview();
            $attempt->msg = $message;
            $attempt->mail = $mail;
            $attempt->save();

            $toEmail = explode(",",setting('site.request_email'));
            foreach ($toEmail as $key => $value) {
                Mail::to($value)->send(new ReviewNo($feed));
            }

        } catch (Exception $e) {
            return response('There was an error while sending your message, please try again later', 200)
                  ->header('Content-Type', 'text/plain');
        }
        return view('negativerev');
    }
}
