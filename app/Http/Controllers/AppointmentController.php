<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Formcontact;
use App\Mail\AppointmentMailer;
use App\Mail\ContactMailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class AppointmentController extends Controller
{
    public function index(Request $request){
        $request->validate([
            'firstname' => 'required|max:255',
            'email' => 'required|max:255|email',
        ]);
        //Email validation
        $params = array(
            "address" => $request->email
        );
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'api:key-4d971e4310b4ccf35e732e0a71033174');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v4/address/validate');
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);

        if($result->risk=="high" || $result->risk=="unknown"){
            return Redirect::back()->with('validation-failed', json_encode([
                'title' => 'All saved.',
                'text' => 'Thank you for the attempt!',
                'icon' => 'success',
            ]));
        }
        try {
            $subscription = new Appointment();
            $subscription->firstname = $request->input('firstname');
            $subscription->lastname = $request->input('lastname');
            $subscription->email = $request->input('email');
            $subscription->subject = $request->input('phone');
            $subscription->service = $request->input('service')?$request->input('service'):"";
            $subscription->message = $request->input('message');
            $subscription->save();

            // $toEmail = explode(",",setting('site.request_email'));

            $feedback = [
                'firstname' => $request->input('firstname'),
                'lastname' => $request->input('lastname'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'service' => $request->input('service'),
                'message' => $request->input('message'),
            ];
            Mail::to('bellesanamedspa@gmail.com')->send(new AppointmentMailer($feedback));
            // foreach ($toEmail as $key => $value) {
            //     Mail::to($value)->send(new AppointmentMailer($feedback));
            // }

        } catch (\Exception $e) {
            return Redirect::back()->with('swal_status', json_encode([
                'title' => 'Appointment attempt failed.',
                'icon' => 'error',
            ]));
        }

        return Redirect::back()->with('sucessapoint', json_encode([
            'title' => 'All saved.',
            'text' => 'Thank you for the attempt!',
            'icon' => 'success',
        ]));
    }


    public function contact(Request $request){
        $request->validate([
            'firstname' => 'required|max:255',
            'email' => 'required|max:255|email',
        ]);
        //Email validation
        $params = array(
            "address" => $request->email
        );
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'api:key-4d971e4310b4ccf35e732e0a71033174');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v4/address/validate');
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);

        if($result->risk=="high" || $result->risk=="unknown"){
            return Redirect::back()->with('validation-failed', json_encode([
                'title' => 'All saved.',
                'text' => 'Thank you for the attempt!',
                'icon' => 'success',
            ]));
        }
        try {
            $subscription = new Formcontact();
            $subscription->firstname = $request->input('firstname');
            $subscription->lastname = $request->input('lastname');
            $subscription->email = $request->input('email');
            $subscription->subject = $request->input('phone');
            $subscription->date = $request->input('date')?$request->input('date'):"";
            $subscription->service = $request->input('service')?$request->input('service'):"";
            $subscription->message = $request->input('message');
            $subscription->save();

            // $toEmail = explode(",",setting('site.request_email'));

            $feedback = [
                'firstname' => $request->input('firstname'),
                'lastname' => $request->input('lastname'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'service' => $request->input('service'),
                'message' => $request->input('message'),
                'date' => $request->input('date'),
            ];
            Mail::to('bellesanamedspa@gmail.com')->send(new ContactMailer($feedback));
            // foreach ($toEmail as $key => $value) {
            //     Mail::to($value)->send(new ContactMailer($feedback));
            // }

        } catch (\Exception $e) {
            return Redirect::back()->with('swal_status', json_encode([
                'title' => 'Appointment attempt failed.',
                'icon' => 'error',
            ]));
        }

        return Redirect::back()->with('sucessapoint', json_encode([
            'title' => 'All saved.',
            'text' => 'Thank you for the attempt!',
            'icon' => 'success',
        ]));
    }
}
