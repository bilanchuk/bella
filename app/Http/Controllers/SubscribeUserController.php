<?php

namespace App\Http\Controllers;

use App\Mail\SubscriptionMail;
use App\Subscribed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class SubscribeUserController extends Controller
{
    public function index(Request $request){
        $request->validate([
            'email' => 'required|max:255|email',
        ]);
        //Email validation
        $params = array(
            "address" => $request->email
        );
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'api:key-4d971e4310b4ccf35e732e0a71033174');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v4/address/validate');
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);

        if($result->risk=="high" || $result->risk=="unknown"){
            return Redirect::back()->with('validation-failed', json_encode([
                'title' => 'All saved.',
                'text' => 'Thank you for the attempt!',
                'icon' => 'success',
            ]));
        }

        try {
            $subscription = new Subscribed();
            $subscription->mail = $request->input('email');
            $subscription->save();

            $toEmail = explode(",",setting('site.request_email'));

            $feedback = [
                'email' => $request->input('email'),
            ];

            // foreach ($toEmail as $key => $value) {
            //     Mail::to($value)->send(new SubscriptionMail($feedback));
            // }

        } catch (\Exception $e) {
            return Redirect::back()->with('swal_status', json_encode([
                'title' => 'Subscription failed.',
                'icon' => 'error',
            ]));
        }

        return Redirect::back()->with('sucess', json_encode([
            'title' => 'All saved.',
            'text' => 'Thank you for the subscription!',
            'icon' => 'success',
        ]));
    }
}
