<?php

namespace App\Http\Controllers;

use App\About;
use App\Contact;
use App\Homeabout;
use App\Homebanner;
use App\Http\Middleware\LocaleChange;
use App\Page;
use App\Service;
use App\Slider;
use App\Treatment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class PageController extends Controller
{


    public static $fallback_locale = 'en';
    public function index(){
        $fallback_locale = self::$fallback_locale;
        $localLang = App::getLocale();
        $langSlug = LocaleChange::getLocale();
        $page = Page::where('slug','/')->first()->translate($localLang, $fallback_locale);
        $services = Service::where('home',1)->get()->translate($localLang, $fallback_locale)->take(6);
        $slider = Slider::orderBy('id')->get()->translate($localLang, $fallback_locale);
        $about = Homeabout::orderBy('id')->first()->translate($localLang, $fallback_locale);
        $banner = Homebanner::orderBy('id')->first();
        $contact = Contact::orderBy('id')->first()->translate($localLang, $fallback_locale);
        $services = Service::orderBy('id')->where("title","!=","Example")->get()->translate($localLang, $fallback_locale);
        return view('home',compact('page','slider','services','about','banner','contact','localLang','fallback_locale','langSlug','services'));
    }
    public function about(){
        $fallback_locale = self::$fallback_locale;
        $localLang = App::getLocale();
        $langSlug = LocaleChange::getLocale();
        $page = Page::where('slug','/about')->first()->translate($localLang, $fallback_locale);
        $about = About::orderBy('id')->first()->translate($localLang, $fallback_locale);
        $contact = Contact::orderBy('id')->first()->translate($localLang, $fallback_locale);
        $services = Service::orderBy('id')->where("title","!=","Example")->get()->translate($localLang, $fallback_locale);
        return view('about',compact('page','about','contact','localLang','fallback_locale','langSlug','services'));
    }
    public function contact(){
        $fallback_locale = self::$fallback_locale;
        $localLang = App::getLocale();
        $langSlug = LocaleChange::getLocale();
        $page = Page::where('slug','/contact')->first()->translate($localLang, $fallback_locale);
        $contact = Contact::orderBy('id')->first()->translate($localLang, $fallback_locale);
        $services = Service::orderBy('id')->where("title","!=","Example")->get()->translate($localLang, $fallback_locale);
        return view('contact',compact('page','contact','localLang','fallback_locale','langSlug','services'));
    }
    public function services(){
        $fallback_locale = self::$fallback_locale;
        $localLang = App::getLocale();
        $langSlug = LocaleChange::getLocale();
        $page = Page::where('slug','/services')->first()->translate($localLang, $fallback_locale);
        $services = Service::orderBy('id')->where("title","!=","Example")->get()->translate($localLang, $fallback_locale);
        $contact = Contact::orderBy('id')->first()->translate($localLang, $fallback_locale);
        return view('services',compact('page','services','contact','localLang','fallback_locale','langSlug'));
    }
    public function treatments(){
        $fallback_locale = self::$fallback_locale;
        $localLang = App::getLocale();
        $langSlug = LocaleChange::getLocale();
        $page = Page::where('slug','/treatments')->first()->translate($localLang, $fallback_locale);
        $servicest = Treatment::orderBy('id')->where("title","!=","Example")->get()->translate($localLang, $fallback_locale);
        $contact = Contact::orderBy('id')->first()->translate($localLang, $fallback_locale);
        $services = Service::orderBy('id')->where("title","!=","Example")->get()->translate($localLang, $fallback_locale);
        return view('treatments',compact('page','servicest','contact','localLang','fallback_locale','langSlug','services'));
    }
    public function service($id){
        $fallback_locale = self::$fallback_locale;
        $localLang = App::getLocale();
        $langSlug = LocaleChange::getLocale();
        try {

        $about = Service::where('slugdisplay',$id)->first()->translate($localLang, $fallback_locale);
        } catch (\Throwable $th) {

        }
        if(isset($about)){
            $page = Page::where('slug','/servicepage')->first()->translate($localLang, $fallback_locale);
            $contact = Contact::orderBy('id')->first()->translate($localLang, $fallback_locale);
            $services = Service::orderBy('id')->where("title","!=","Example")->get()->translate($localLang, $fallback_locale);
            return view('service',compact('about','page','contact','localLang','fallback_locale','langSlug','services'));
        }else{
            return redirect('/');
        }

    }
    public function treatment($id){
        $fallback_locale = self::$fallback_locale;
        $localLang = App::getLocale();
        $langSlug = LocaleChange::getLocale();
        $about = Treatment::where('slugdisplay',$id)->first()->translate($localLang, $fallback_locale);
        if($about){
            $page = Page::where('slug','/treatmentpage')->first()->translate($localLang, $fallback_locale);
            $contact = Contact::orderBy('id')->first()->translate($localLang, $fallback_locale);
            $services = Service::orderBy('id')->where("title","!=","Example")->get()->translate($localLang, $fallback_locale);
            return view('treatment',compact('about','page','contact','localLang','fallback_locale','langSlug','services'));
        }else{
            return redirect('/');
        }

    }

    public function rev(){
        return view('/vendor/voyager/rev');
    }

    public function opinion($id){
        $mail = $id;
        return view('opinion',compact('mail'));
    }
}
