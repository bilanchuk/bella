<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Treatment extends Model
{
    use HasFactory;
    use Translatable;
    protected $translatable = ['title', 'subtitle','body','body2','body3','body4','body5','body6','body7','body8','body9','body10','page_title','meta_keywords','meta_description'];
}
