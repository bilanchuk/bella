<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTreatmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treatments', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('subtitle')->nullable();
            $table->string('home')->nullable();
            $table->string('imghome')->nullable();
            $table->string('page_title')->nullable();
            $table->longText('meta_keywords')->nullable();
            $table->longText('meta_description')->nullable();
            $table->longText('body')->nullable();
            $table->longText('slugdisplay')->nullable();
            $table->longText('body2')->nullable();
            $table->longText('body3')->nullable();
            $table->longText('body4')->nullable();
            $table->longText('body5')->nullable();
            $table->longText('body6')->nullable();
            $table->longText('body7')->nullable();
            $table->longText('body8')->nullable();
            $table->longText('body9')->nullable();
            $table->longText('body10')->nullable();
            $table->integer('hide')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('treatments', function (Blueprint $table) {
            //
        });
    }
}
