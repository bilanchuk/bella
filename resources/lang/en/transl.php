<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return [

    'Home' => 'Home',
    'servicestitle' => 'Our Services',
    'servicestext' => 'Bella Sana offers a wide range of skin care services at very affordable prices. Among our most popular procedures are laser hair removal, laser spider vein removal, HydraFacial, TruSculpt 3D, scar removal, laser skin rejuvenation, Botox, Juvederm, vitamin B12 injections, microdermabrasion, oxygenating Facial, and others. Schedule your procedure at our med spa today!',
    'about' => 'About Us',
    'welcome' => 'Welcome to Bella Sana!',
    'newsleter' => 'Newsletter sign up',
    'subscribe' => 'Subscribe to receive our newest promotions and special deals',
    'location' => '1400. Golf RD #201, Des Plaines, II, 60016, USA',
    'working' => 'Working hours',
    'monday-friday' => 'Monday – Friday',
    'saturday' => 'Saturday',
    'sunday' => 'Sunday',
    'copyright' => 'All Rights Reserved',
    'team' => 'Our Team',
    'services' => 'Services',
    'contact' => 'Contact Us',
    'appoint' => 'Make an appointment',
    'more' => 'More',
    'readmore' => 'Read more',
    'moreservices' => 'More Services',
    'youremail' => 'Your email address',
    'subscribe' => 'Subscribe',
    'fname' => 'Full name',
    'lname' => 'Last name',
    'date' => 'Date and time',
    'subject' => 'Subject',
    'service' => 'Service',
    'other' => 'other',
    'msg' => 'Message',
    'send' => 'Send email',
    'question' => 'Have a Question?',
    'email' => 'Email',
    'phone' => 'Phone Number',
    'contactonappoint' => 'Contact Us',
    'closed' => 'Closed',
    'Our Procedures' => 'Our Procedures',
    'treatments' => 'Treatments',
    'treattitle' => 'Our Treatments',


];

