<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return [

    'Home' => 'Главная',
    'servicestitle' => 'Наши услуги',
    'servicestext' => 'SPA-салон «Bella Sana» предлагает широкий спектр услуг по уходу за кожей по очень доступным ценам. Среди наших самых популярных процедур представлены: лазерная эпиляция, лазерное удаление сосудистых звездочек, «HydraFacial, «TruSculpt 3D», удаление рубцов, лазерное омоложение кожи, ботокс, Ювидерм, инъекции витамина B12, микрошлифовка кожи, оксигенация лица и другие. Запишитесь на процедуру в нашем медицинском SPA-салоне «Bella Sana» уже сегодня!',
    'about' => 'О нас',
    'welcome' => 'Добро пожаловать в наш SPA-салон «Bella Sana»',
    'newsleter' => 'ПОДПИСАТЬСЯ НА НОВОСТИ',
    'subscribe' => 'Подпишитесь, чтобы получать нашу новостную рассылку',
    'location' => '1400. Golf RD #201, Des Plaines, II, 60016, USA',
    'working' => 'График работы',
    'monday-friday' => 'Понедельник - пятница',
    'saturday' => 'Суббота',
    'sunday' => 'Воскресенье',
    'copyright' => 'All Rights Reserved',
    'team' => 'НАША КОМАНДА',
    'services' => 'Услуги',
    'contact' => 'Контакты',
    'appoint' => 'Записаться на прием',
    'more' => 'More',
    'readmore' => 'Подробнее',
    'moreservices' => 'Больше услуг',
    'youremail' => 'Ваш адрес электронной почты',
    'subscribe' => 'Подписаться',
    'fname' => 'Имя',
    'lname' => 'Фамилия',
    'date' => 'Date and time',
    'subject' => 'Subject',
    'service' => 'Услуга',
    'other' => 'other',
    'msg' => 'Сообщение',
    'send' => 'Отправить сообщение',
    'question' => 'Получить помощь',
    'email' => 'Эл. адрес',
    'phone' => 'Номер телефона',
    'contactonappoint' => 'СВЯЗАТЬСЯ С НАМИ',
    'closed' => 'выходной',
    'Our Procedures' => 'Наши процедуры',
    'treatments' => 'Лечение',
    'treattitle' => 'Наши Методы Лечения',

];

