<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return [

    'Home' => 'Inicio',
    'servicestitle' => 'Nuestros servicios',
    'servicestext' => 'Bella Sana ofrece una amplia gama de servicios para el cuidado de la piel a precios muy asequibles. Entre nuestros procedimientos más populares se encuentran la depilación láser, la eliminación de arañas vasculares con láser, HydraFacial, TruSculpt 3D, eliminación de cicatrices, rejuvenecimiento cutáneo con láser, Botox, Juvederm, inyecciones de vitamina B12, microdermoabrasión, facial oxigenante y otros. ¡Programe su procedimiento en nuestro spa médico hoy mismo!',
    'about' => 'SOBRE NOSOTROS',
    'welcome' => 'Bienvenidos a to Bella Sana!',
    'newsleter' => 'INSCRÍBASE AL BOLETÍN',
    'subscribe' => 'Suscríbase para recibir nuestras promociones más recientes y ofertas especiales',
    'location' => 'Golf Rd # 201, Des Plaines IL, 60016, EE. UU.',
    'working' => 'Horas Laborales',
    'monday-friday' => 'Lunes - Viernes',
    'saturday' => 'Sábado',
    'sunday' => 'Domingo',
    'copyright' => 'Reservados todos los derechos',
    'team' => 'NUESTRO EQUIPO',
    'services' => 'Servicios',
    'contact' => 'Contáctenos',
    'appoint' => 'Concertar cita',
    'more' => 'Más',
    'readmore' => 'Lee mas',
    'moreservices' => 'Más servicios',
    'youremail' => 'Su dirección de correo electrónico',
    'subscribe' => 'Suscribir',
    'fname' => 'Nombre propio',
    'lname' => 'Apellido',
    'date' => 'Fecha y hora',
    'subject' => 'Sujeto',
    'service' => 'Servicio',
    'other' => 'otro',
    'msg' => 'Mensaje',
    'send' => 'Enviar correo electrónico',
    'question' => 'Have a Question?',
    'email' => 'Email',
    'phone' => 'Phone Number',
    'contactonappoint' => 'Contact Us',
    'closed' => 'Closed',
    'Our Procedures' => 'Our Procedures',
    'treatments' => 'Trato',
    'treattitle' => 'Nuestros Tratamientos',

];

