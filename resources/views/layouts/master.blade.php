<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="no-js">
  <head>
	<!-- Basic Page Needs

     ================================================== -->

	 <meta charset="utf-8">

	 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">



     <title>@yield('title')</title>

     <meta name="description" content="@yield('meta_description')">

     <meta name="keywords" content="@yield('meta_keyword')">


	 <!-- Mobile Specific Metas

     ================================================== -->

   <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-K8LZ4N96GV"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-K8LZ4N96GV');
</script>

     <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">




        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/ionicons.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/css-plugins-call.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/bundle.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/main.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/colors.css')}}">
        <link rel="shortcut icon" href="{{Voyager::image('favicon1.png')}}" type="image/x-icon">

        @if ($page->slug=="/" && $langSlug=="")
            <link rel="canonical" href="https://bellasanamedspa.com/"/>
            <link rel="alternate" hreflang="en" href="https://bellasanamedspa.com/" />
            <link rel="alternate" hreflang="es" href="https://bellasanamedspa.com/es" />
            <link rel="alternate" hreflang="ru" href="https://bellasanamedspa.com/ru" />
            <link rel="alternate" hreflang="x-default" href="https://bellasanamedspa.com/" />
        @endif
        @if ($page->slug=="/" && $langSlug=="es")
        <link rel="canonical" href="https://bellasanamedspa.com/es"/>
        <link rel="alternate" hreflang="en" href="https://bellasanamedspa.com/" />
        <link rel="alternate" hreflang="es" href="https://bellasanamedspa.com/es" />
        <link rel="alternate" hreflang="ru" href="https://bellasanamedspa.com/ru" />
        <link rel="alternate" hreflang="x-default" href="https://bellasanamedspa.com/" />
    @endif
    @if ($page->slug=="/" && $langSlug=="ru")
    <link rel="canonical" href="https://bellasanamedspa.com/ru"/>
    <link rel="alternate" hreflang="en" href="https://bellasanamedspa.com/" />
    <link rel="alternate" hreflang="es" href="https://bellasanamedspa.com/es" />
    <link rel="alternate" hreflang="ru" href="https://bellasanamedspa.com/ru" />
    <link rel="alternate" hreflang="x-default" href="https://bellasanamedspa.com/" />
@endif
@if ($page->slug=="/servicepage" && $langSlug=="")
<link rel="canonical" href="https://bellasanamedspa.com/service/{{$about->slugdisplay}}"/>
<link rel="alternate" hreflang="en" href="https://bellasanamedspa.com/service/{{$about->slugdisplay}}" />
<link rel="alternate" hreflang="es" href="https://bellasanamedspa.com/es/service/{{$about->slugdisplay}}" />
<link rel="alternate" hreflang="ru" href="https://bellasanamedspa.com/ru/service/{{$about->slugdisplay}}" />
<link rel="alternate" hreflang="x-default" href="https://bellasanamedspa.com/service/{{$about->slugdisplay}}" />

@endif
@if ($page->slug=="/servicepage" && $langSlug=="es")
<link rel="canonical" href="https://bellasanamedspa.com/es/service/{{$about->slugdisplay}}"/>
<link rel="alternate" hreflang="en" href="https://bellasanamedspa.com/service/{{$about->slugdisplay}}" />
<link rel="alternate" hreflang="es" href="https://bellasanamedspa.com/es/service/{{$about->slugdisplay}}" />
<link rel="alternate" hreflang="ru" href="https://bellasanamedspa.com/ru/service/{{$about->slugdisplay}}" />
<link rel="alternate" hreflang="x-default" href="https://bellasanamedspa.com/service/{{$about->slugdisplay}}" />

@endif
@if ($page->slug=="/servicepage" && $langSlug=="ru")
<link rel="canonical" href="https://bellasanamedspa.com/ru/service/{{$about->slugdisplay}}"/>
<link rel="alternate" hreflang="en" href="https://bellasanamedspa.com/service/{{$about->slugdisplay}}" />
<link rel="alternate" hreflang="es" href="https://bellasanamedspa.com/es/service/{{$about->slugdisplay}}" />
<link rel="alternate" hreflang="ru" href="https://bellasanamedspa.com/ru/service/{{$about->slugdisplay}}" />
<link rel="alternate" hreflang="x-default" href="https://bellasanamedspa.com/service/{{$about->slugdisplay}}" />

@endif

<link rel="stylesheet" href="{{asset('assets/css/fancybox.css')}}" />

    </head>

    <body>

        @include('partials.header')

        @yield('content')

        @include('partials.footer')



     <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <!-- jQuery Local -->
        <script src="{{asset('assets/js/jquery-3.2.1.min.js')}}"></script>

        <!-- Popper min js -->
        <script src="{{asset('assets/js/popper.min.js')}}"></script>
        <!-- Bootstrap min js  -->
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
		<!-- nivo slider pack js  -->
        <script src="{{asset('assets/js/jquery.nivo.slider.pack.js')}}"></script>
        <!-- All plugins here -->
        <script src="{{asset('assets/js/plugin.min.js')}}"></script>
        <!-- Main js  -->
        <script src="{{asset('assets/js/sweetalert.min.js')}}"></script>
        <script src="{{asset('assets/js/main.min.js')}}"></script>
        <script src="{{asset('assets/js/fancybox.umd.js')}}"></script>



        @if ($page->slug=='/servicepage')
            <script>
                $(document).ready(function () {
                    $(".container-img-text a img").each(function(){
                        var newyoutube = `  <div class="row container-img-text">
        <a data-fancybox="video-gallery" href="${$(this).parent().attr("href")}" class="popup-youtube relative theme video-play-button item-center">
            <img style="${$(this).attr("style")}" src="${$(this).attr("src")}" alt="">
            <i class="fa fa-play"></i>
          </a>
    </div>`;
    $(this).replaceWith(newyoutube);
                    })
                });
            </script>
        @endif



    </body>



</html>
