@extends('layouts.master')
@section('title', $page->title)
@section('meta_keyword', $page->meta_keywords )
@section('meta_description', $page->meta_description)

@section('content')

@include('components.bread')

<!-- poslistcategories -->
<div class="poslistcategories">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="pos_title_categories">
                    <h1><span>{{trans('transl.treattitle')}}</span></h1>
                    <p>{{trans('transl.servicestext')}}</p>
                </div>
            </div>
        </div>
        <div class="row pos_content">
            @foreach ($servicest as $item)
                <div class="col-md-4">
                    <div class="item-listcategories">
                        <div class="list-categories">
                            <div class="box-inner">
                                <div class="thumb-category">
                                    <a href="{{url($langSlug.'/treatment/'.$item->slugdisplay)}}">
                                        <img src="{{Voyager::image($item->imghome)}}" alt="">
                                    </a>
                                </div>
                                <div class="desc-listcategoreis">
                                    <h3 class="name_categories"><a href="{{url($langSlug.'/treatment/'.$item->slugdisplay)}}">{{$item->title}}</a></h3>
                                    <p class="description-list">{{$item->subtitle}}</p>
                                    <div class="listcate_shop_now">
                                        <a href="{{url($langSlug.'/treatment/'.$item->slugdisplay)}}">{{trans('transl.readmore')}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach

            <div class="about__us_page_area" style="width: 100%;
                display: flex;
                justify-content:center;">
                    <div class="wpb_wrapper">
                        <p><a href="{{url($langSlug."/".App\Page::where('slug','/contact')->value('slugdisplay'))}}">{{trans('transl.appoint')}}</a></p>
                    </div>
                </div>


        </div>
    </div>
</div>
<!-- poslistcategories end -->

@include('components.newsletter')
@include('components.appoint')

@endsection
