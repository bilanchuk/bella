
@if ($errors->first('email'))
        <script>
            swal({
                title: "Subscription Error!",
                text: "{{ $errors->first('email') }}",
                icon: "error",
                buttons: ["Cancel", "Go to subscription form"],
            }).then(() => {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#subscription").offset().top
                }, 500);
            });
        </script>
    @endif
    @if (Session::has('sucess'))
        <script>
            swal({
                title: "All saved",
                text: "Thank you for the subscription!",
                icon: "success",
                button: "Close!",
            }).then(() => {
                    $([document.documentElement, document.body]).animate({
                        scrollTop: $("#subscription").offset().top
                    }, 500);
                });
        </script>
    @endif
    @if (Session::has('validation-failed'))
    <script>
        swal({
            title: "Error.",
            text: "Please write correct e-mail address!",
            icon: "error",
            button: "Close!",
        }).then(() => {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#contact-page-form").offset().top
                }, 500);
            });
    </script>
@endif
<div class="container newsletcont" id="subscription" style="margin-top: 140px; background-color: #F6F6F6; padding: 2px;">
    <div class="rows">
        <div class="ft_newsletter">
            <div id="block-newsletter-label">
                <div class="title-newsletter">
                    <h2>{{trans('transl.newsleter')}}</h2>
                    <p class="desc">(Subscribe to our Newsletter.)</p>
                </div>
            </div>
            <form action="{{url('news')}}" method="POST">
                @csrf
                <input class="btn btn-primary float-xs-right hidden-xs-down" name="submitNewsletter" value="{{trans('transl.subscribe')}}" type="submit">
                <div class="input-wrapper">
                  <input name="email" value="" placeholder="{{trans('transl.youremail')}}" aria-labelledby="block-newsletter-label" type="text">
                </div>
            </form>
        </div>

    </div>
</div>
