<!-- top breadcrumb -->
<div class="top_breadcrumb">
    <div class="breadcrumb_container ">
        <div class="container">
            <nav data-depth="3" class="breadcrumb">
                <ol>
                    <li>
                      <a href="{{url($langSlug.'/')}}">
                        <span>{{trans('transl.Home')}}</span>
                      </a>
                    </li>
                    @switch($page->slug)
                        @case('/servicepage')
                        <li>
                            <a href="{{url($langSlug."/".App\Page::where('slug','/services')->value('slugdisplay'))}}">
                            <span>{{trans('transl.services')}}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url($langSlug.'/service/'.$about->slugdisplay)}}">
                            <span>{{$about->title}}</span>
                            </a>
                        </li>
                            @break
                        @case('/about')
                        <li>
                            <a href="{{url($langSlug."/".$page->slugdisplay)}}">
                            <span>About US</span>
                            </a>
                        </li>
                            @break
                            @case('/contact')
                            <li>
                                <a href="{{url($langSlug."/".$page->slugdisplay)}}">
                                <span>Contact Us</span>
                                </a>
                            </li>
                                @break
                                @case('/services')
                                <li>
                                    <a href="{{url($langSlug."/".$page->slugdisplay)}}">
                                    <span>Services</span>
                                    </a>
                                </li>
                                    @break
                        @default
                        <li>
                            <a href="{{url($langSlug."/".$page->slugdisplay)}}">
                            <span>{{$page->title}}</span>
                            </a>
                        </li>
                    @endswitch


                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- top breadcrumb end -->
