

<script src="https://www.google.com/recaptcha/api.js?hl=en" async defer></script>
@if ($errors->hasAny(['firstname', 'lastname', 'email','subject','message','captcha']))
<script>
    swal({
        title: "Appointment Attempt Error!",
        text: "{{ $errors->all()[0] }}",
        icon: "error",
        buttons: ["Cancel", "Go to appointment form"],
    }).then(() => {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#contact-page-form").offset().top
        }, 500);
    });
</script>
@endif
@if (Session::has('sucessapoint'))
    <script>
        swal({
            title: "We\'ve got your message.",
            text: "We wil return with response as soon as possible!",
            icon: "success",
            button: "Close!",
        }).then(() => {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#contact-page-form").offset().top
                }, 500);
            });
    </script>
@endif
@if (Session::has('validation-failed'))
    <script>
        swal({
            title: "Error.",
            text: "Please write correct e-mail address!",
            icon: "error",
            button: "Close!",
        }).then(() => {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#contact-page-form").offset().top
                }, 500);
            });
    </script>
@endif

<div class="container">
    <div class="contact-form-area contact-none-border" id="contact-page-form">
        <div class="container" style="padding-left: 0px;
        padding-right: 0px;">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12">
                    <div class="contact-form-inner">
                        <h2>{{trans('transl.question')}}</h2>
                        <form action="{{url('/appoint')}}" method="post" id="register-form-ersag">
                            @csrf
                            <div class="row">
                                <div class="col">
                                    <input type="text" name="firstname" class="form-control" placeholder="{{trans('transl.fname')}}*" required>
                                </div>
                                <div class="col">
                                    <input type="text" name="lastname" class="form-control" placeholder="{{trans('transl.lname')}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <input type="text" name="email" class="form-control" placeholder="{{trans('transl.email')}}*" required>
                                </div>
                                <div class="col">
                                    <input type="text" name="phone" class="form-control" placeholder="{{trans('transl.phone')}}*" required>
                                </div>


                            </div>
                            <div class="row">

                                <div class="col">
                                    <select name="service" id="service">
                                        <option value="" selected disabled hidden>{{trans('transl.service')}}</option>
                                        @foreach ($service as $item)
                                            <option value="{{$item->title}}">{{$item->title}}</option>
                                        @endforeach
                                        <option value="Other">{{trans('transl.other')}}</option>
                                     </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required"
                                        aria-invalid="false" placeholder="{{trans('transl.msg')}}"></textarea>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 15px">
                                <div class="col">
                                    <div class="g-recaptcha" data-type="image" data-sitekey="6LfUnYAaAAAAAJNJqauHflFarp7miiHLBwfHPnGQ"></div>

                                </div>
                                <div class="col"></div>
                            </div>
                            <div class="contact-submit">
                                <input type="submit" value="{{trans('transl.send')}}" class="wpcf7-form-control wpcf7-submit button" id="submit-form-id" @if ($langSlug=="es")
                                    style="width: 272px;"
                                @endif>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12">
                    <div class="contact-address-area">
                        <h2 style="color: #434343;">{{trans('transl.contactonappoint')}}</h2>
                        <p>{{$contact->text}}</p>
                        <ul>
                            <li>
                                <i class="fa fa-map-marker">&nbsp;</i>{{setting('site.location')}}</li>
                            <li>
                                <i class="fa fa-phone">&nbsp;</i>{{setting('site.phone')}}</li>
                            <li>
                                <i class="fa fa-envelope-o"></i>&nbsp;</i><img style="position: relative;
                                right: 7px;" src="{{Voyager::image('photo_2020-11-18_16-02-53.jpg')}}" alt=""></li>
                        </ul>
                        <h3>
                            <strong>{{trans('transl.working')}}</strong>
                        </h3>
                        <p>
                            <strong>{{trans('transl.monday-friday')}}</strong>: &nbsp;10:00 AM – 6:00PM <br>
                            <strong>{{trans('transl.saturday')}}</strong>: &nbsp;10:00 AM – 4:00PM <br>
                            <strong>{{trans('transl.sunday')}}</strong>: &nbsp;{{trans('transl.closed')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    try {
        var dtt = document.getElementById('ddt')
    dtt.onfocus = function (event) {
        this.type = 'datetime-local';
        this.focus();
    }
    } catch (error) {

    }

    var form = document.getElementById('register-form-ersag');
    document.getElementById("submit-form-id").addEventListener('click', function(event){
    var res = grecaptcha.getResponse();
    if(res==""){
        event.preventDefault();
        swal({
        title: "Please confirm that you are not a robot!",
        text: "Сheck the box",
        icon: "error",
        button: "Go to appointment form",
    }).then(() => {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#contact-page-form").offset().top
        }, 500);
    });
    }
});
  </script>
