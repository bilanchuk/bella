<div class="wrapper home-one">
    <header class="header-area">

        <!-- Header bottom area start -->
        <div class="header-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="header_logo col-left col-lg-2 col-xl-2 col-md-12 col-xs-12">
                        <a href="{{url($langSlug.'/')}}" id="logo-image-id"><img src="{{Voyager::image(setting('site.logo'))}}"  alt="harosa" style="
                        "></a>
                        <div class="blockcart cart-preview pull-right phone-head-only">
                            <a href="tel:{{setting('site.phone')}}" class="icons-head"><img src="{{Voyager::image("img_108606.png")}}" alt=""></a>
                            <a href="{{url($langSlug."/".App\Page::where('slug','/contact')->value('slugdisplay'))}}" class="icons-head"><img src="{{Voyager::image("business-and-finance-outline-33-18-512.png")}}" alt=""></a>
                            <div class="switcher">
                                <!-- language-menu -->
                                <div class="language">
                                    <a href="javascript:;">
                                        {{$localLang}}
                                        <i class="ion-ios-arrow-down" style="margin-left: 5px;"></i>
                                    </a>
                                    <ul style="z-index: 100;">
                                        @if($localLang != 'en')
                                            <li>
                                                <a href="{{route('setlocale', ['lang' => 'en']) }}">
                                                    <span>EN</span>
                                                </a>
                                            </li>
                                        @endif


                                        @if($localLang != 'es')
                                            <li>
                                                <a href="{{route('setlocale', ['lang' => 'es']) }}">
                                                    <span>ES</span>
                                                </a>
                                            </li>
                                        @endif

                                        @if($localLang != 'pl')
                                            <li>
                                                <a href="{{route('setlocale', ['lang' => 'pl']) }}">
                                                    <span>PL</span>
                                                </a>
                                            </li>
                                        @endif
                                        @if($localLang != 'ru')
                                            <li>
                                                <a href="{{route('setlocale', ['lang' => 'ru']) }}">
                                                    <span>RU</span>
                                                </a>
                                            </li>
                                        @endif
                                        @if($localLang != 'ua')
                                            <li>
                                                <a href="{{route('setlocale', ['lang' => 'ua']) }}">
                                                    <span>UA</span>
                                                </a>
                                            </li>
                                        @endif


                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-right col-xs-12 col-lg-10 col-xl-10 col-md-12 display_top" style="z-index: 2;">
                        <!-- main-menu -->
                        <div class="main-menu" style="padding-left: 15px">
                            <nav>
                                <ul>
                                    <li @if ($page->slug=="/")
                                        class="current"
                                    @endif><a href="{{url($langSlug.'/')}}">{{trans('transl.Home')}}</a>
                                    </li>
                                    <li @if ($page->slug=="/about")
                                        class="current"
                                    @endif><a href="{{url($langSlug."/".App\Page::where('slug','/about')->value('slugdisplay'))}}">{{trans('transl.about')}}</a></li>
                                    <li @if ($page->slug=="/services" || $page->slug=="/servicepage")
                                        class="current"
                                    @endif><a href="{{url($langSlug."/".App\Page::where('slug','/services')->value('slugdisplay'))}}">{{trans('transl.services')}} <i class="fa fa-angle-down"></i></a>
                                        <ul class="submenu">
                                            @foreach ($services as $item)
                                                <li><a href="{{url($langSlug.'/service/'.$item->slugdisplay)}}">{{$item->title}}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    <li @if ($page->slug=="/treatments" || $page->slug=="/treatmentpage")
                                        class="current"
                                    @endif ><a href="{{url($langSlug."/".App\Page::where('slug','/treatments')->value('slugdisplay'))}}">{{trans('transl.treatments')}} <i class="fa fa-angle-down"></i></a>
                                        <ul class="submenu">
                                            @foreach ($treatments as $item)
                                                <li><a href="{{url($langSlug.'/treatment/'.$item->slugdisplay)}}">{{$item->title}}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    <li @if ($page->slug=="/contact")
                                        class="current"
                                    @endif><a href="{{url($langSlug."/".App\Page::where('slug','/contact')->value('slugdisplay'))}}">{{trans('transl.contact')}}</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="mobile-menu-area">
                            <div class="mobile-menu">
                                <nav id="mobile-menu-active">
                                    <ul class="menu-overflow">
                                        <li class="current"><a href="{{url($langSlug.'/')}}">{{trans('transl.Home')}} </a>

                                        </li>
                                        <li><a href="{{url($langSlug."/".App\Page::where('slug','/about')->value('slugdisplay'))}}">{{trans('transl.about')}}</a></li>
                                        <li><a href="{{url($langSlug."/".App\Page::where('slug','/services')->value('slugdisplay'))}}">{{trans('transl.services')}}</a>
                                            <ul class="">
                                                @foreach ($services as $item)
                                                    <li><a href="{{url($langSlug.'/service/'.$item->slugdisplay)}}">{{$item->title}}</a></li>
                                                @endforeach
                                            </ul></li>
                                            <li @if ($page->slug=="/treatments" || $page->slug=="/treatmentpage")
                                                class="current"
                                            @endif ><a href="{{url($langSlug."/".App\Page::where('slug','/treatments')->value('slugdisplay'))}}">{{trans('transl.treatments')}}</a>
                                                <ul class="">
                                                    @foreach ($treatments as $item)
                                                        <li><a href="{{url($langSlug.'/treatment/'.$item->slugdisplay)}}">{{$item->title}}</a></li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        <li><a href="{{url($langSlug."/".App\Page::where('slug','/contact')->value('slugdisplay'))}}">{{trans('transl.contact')}}</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="blockcart cart-preview pull-right">
                            <a href="tel:{{setting('site.phone')}}"><i class="fa fa-phone self-fa"></i></a>
                            <a href="{{url($langSlug."/".App\Page::where('slug','/contact')->value('slugdisplay'))}}" class="appoint">{{trans('transl.appoint')}}</a>
                            <div class="switcher">
                                <!-- language-menu -->
                                <div class="language">
                                    <a href="javascript:;">
                                        {{$localLang}}
                                        <i class="ion-ios-arrow-down" style="margin-left: 5px;"></i>
                                    </a>
                                    <ul>
                                        @if($localLang != 'en')
                                            <li>
                                                <a href="{{route('setlocale', ['lang' => 'en']) }}">
                                                    <span>EN</span>
                                                </a>
                                            </li>
                                        @endif


                                        @if($localLang != 'es')
                                            <li>
                                                <a href="{{route('setlocale', ['lang' => 'es']) }}">
                                                    <span>ES</span>
                                                </a>
                                            </li>
                                        @endif

                                        @if($localLang != 'ru')
                                        <li>
                                            <a href="{{route('setlocale', ['lang' => 'ru']) }}">
                                                <span>RU</span>
                                            </a>
                                        </li>
                                        @endif

                                        @if($localLang != 'pl')
                                            <li>
                                                <a href="{{route('setlocale', ['lang' => 'pl']) }}">
                                                    <span>PL</span>
                                                </a>
                                            </li>
                                        @endif

                                        @if($localLang != 'ua')
                                            <li>
                                                <a href="{{route('setlocale', ['lang' => 'ua']) }}">
                                                    <span>UA</span>
                                                </a>
                                            </li>
                                        @endif


                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Header bottom area end -->
    </header>
</div>
