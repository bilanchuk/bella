<footer>
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="copyright-inner">
                        <div class="row">
                            <div class="col-sm-12 col-lg-6 col-md-6">
                                <p>Copyright © {{now()->year}} <a href="https://morganmadison.marketing" target="_blank">Morgan & Madison Marketing</a> . {{trans('transl.copyright')}}.</p>
                            </div>
                            <div class="col-sm-12 col-lg-6 col-md-6 pull-right">
                                <div class="social_follow">
                                    <ul>
                                        <li class="facebook"><a href="{{setting('site.facebook')}}" target="_blank">Facebook</a></li>
                                        <li class="youtube"><a href="{{setting('site.youtube')}}" target="_blank">YouTube</a></li>
                                        <li class="instagram"><a href="{{setting('site.instagram')}}" target="_blank">Instagram</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
