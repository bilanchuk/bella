@extends('voyager::master')
@section('content')
  <h1 style="text-align: center">Review Request</h1>

  <form action="/sendrequest" method="post" style="width: 60%; text-align:center; margin:0 auto; margin-top:40px; margin-bottom:40px;">
    @csrf
    <div class="for-group">
        <h4 class="label-mail">Name</h4>
        <input type="text" name="name" class="form-control" placeholder="Write the recepient name">
    </div>
    <div class="form-group">
        <h4 class="label-mail" >E-mail</h4>
        <input type="text" name="mail" placeholder="Write the recepient e-mail" class="form-control">
    </div>
    <input type="submit" value="Send"  class="btn btn-success">

  </form>
  <div class="success" id="message-app" style="<?php if(\Session::has('swaled_status') || $errors->any()){
    echo "display:block;";
} else{
    echo "display: none;";
} ?>">
     @if (\Session::has('swaled_status'))
     <?php $status = json_decode(Session::get('swaled_status')); ?>
     <div class="alert alert-{{$status->icon}}">
         <ul>
             <li>{!! $status->title !!}</li>
             <li>{!! $status->text !!}</li>
         </ul>
     </div>
 @endif
 @if ($errors->any())
     <div class="alert alert-danger">
         <ul>
             @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
             @endforeach
         </ul>
     </div>
 @endif
</div>


  <script>
      window.onscroll = function () { window.scrollTo(0, 0); };
  </script>
@stop
