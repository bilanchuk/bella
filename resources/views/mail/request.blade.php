<div class="wrapper" style="
padding-left: 10px;
padding-top: 40px;
padding-right: 40px;
position: relative;">
    <div class="img-wrap" style="width: 100%;
    /* max-height: 400px; */
    height: 400px;">
        <img src="{{Voyager::image('doctor.png')}}" alt="" style="-o-object-fit: contain;object-fit: contain;
        height: 100%;
        width: 100%;
        border: 1px solid #bebebe;
        border-radius: 3px;">
    </div>
    <div class="text-wrap" style="width: 100%;
    /* padding-left: 15px; */
    position: relative;">
        <p style="font-size: 34px;
        line-height: 36px;
        font-weight: 600;
        color: #c36e99;
        text-align: center;
        margin-bottom: 15px !important;
        margin-top: 23px !important;">Dear {{$feedback['name']}}</p>
        <p style="font-size: 21px;
        line-height: 28px;
        font-weight: 500;
        color: #000000;
        text-align: center;
        margin-top: 10px !important;
        margin-bottom: 46px;
        ">Thank you for choosing Bella Sana. We highly
            appreciate your business  and want to make sure that  you are satisfied with the services provided to you.</p>
        <p style="text-align: center;">
            <a href="{{URL::to('/opinion/'.$feedback['mail'])}}" style="background-color: #c36e99;
            border-radius: 10px;
            font-weight: 400;
            color: #ffffff;
            font-size: 13px;
            text-decoration: none;
            padding: 14px;
    ">Share your experience</a>
        </p>


    </div>
</div>
<div class="testimonials" style="margin-top: 15px; padding-right: 40px; padding-left: 20px;">
    <img src="{{Voyager::image('logo_250_х78.png')}}" alt="" style="width: 250px; height: 78px; display: block;
    margin-left: auto;
    margin-right: auto;
    margin-top: 30px;">
    <div style="font-size: 21px;
    line-height: 26px;
    font-weight: 300;
    color: #4e4e4e;margin-top: 20px; text-align:center;">
        (847) 298-3338 <br>
        1400 Golf Rd #201, Des Plaines, IL 60016 <br>
        <a href="http://www.bellasanamedspa.com/" target="_blank" style="text-decoration: none; color: #4e4e4e;">www.bellasanamedspa.com</a>

    </div>
</div>






