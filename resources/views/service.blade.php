@extends('layouts.master')
@section('title', $about->page_title)
@section('meta_keyword', $about->meta_keywords )
@section('meta_description', $about->meta_description)

@section('content')
@include('components.bread')
<div class="container about-page-select">


    @if ($about->body!="" && strip_tags($about->body)!="-")
    <div class="row container-img-text">

            {!! $about->body !!}

    </div>
    @endif
    @if ($about->body2!="" && strip_tags($about->body2)!="-")
    <div class="row container-img-text">

        {!! $about->body2 !!}


    </div>
    @endif
    @if ($about->body3!="" && strip_tags($about->body3)!="-")
    <div class="row container-img-text">
        {!! $about->body3 !!}
    </div>
    @endif
    @if ($about->body4!="" && strip_tags($about->body4)!="-")
    <div class="row container-img-text">
        <div class="col-md-12 text-full-size">
            {!! $about->body4 !!}
        </div>

    </div>
    @endif
    @if ($about->body5!="" && strip_tags($about->body5)!="-")
    <div class="row container-img-text">
        {!! $about->body5 !!}
    </div>
    @endif
    @if ($about->body6!="" && strip_tags($about->body6)!="-")
    <div class="row container-img-text">
        {!! $about->body6 !!}

    </div>
    @endif
    @if ($about->body7!="" && strip_tags($about->body7)!="-")
    <div class="row container-img-text">
        {!! $about->body7 !!}
    </div>
    @endif
    @if ($about->body8!="" && strip_tags($about->body8)!="-")
    <div class="row container-img-text">
        {!! $about->body8 !!}
    </div>
    @endif
    @if ($about->body9!="" && strip_tags($about->body9)!="-")
    <div class="row container-img-text">
        {!! $about->body9 !!}

    </div>
    @endif
    @if ($about->body10!="" && strip_tags($about->body10)!="-")
    <div class="row container-img-text">
        <div class="col-md-12">

            {!! $about->body10 !!}
        </div>


    </div>
    @endif
</div>
<div class="about-page-area">

    <div class="about__us_page_area">
        <div class="container">
            <div class="row">



            </div>
        </div>
    </div>

    @include('components.appoint')
</div>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script>
    $(document).ready(()=>{
        $("h2,h1,h3, h2 span").css({"font-size": "30px",
    "line-height": "38px",
    "font-weight": "400",
    "text-transform": "uppercase",
    "color": "#c36e99",
    "font-family": "'PT Serif', serif"});
    $("p").css({"color": "#6f6f6f",
    "font-size": "15px"});
    $("img:only-child").slice(1).css({"width":"100%"});
    // $(".container img").slice(1,3).css({
    // "width": "50%"});
    // $(".container img").slice(1,2).css({
    // "margin-right": "80px"});
    // $(".container img").slice(2,3).css({
    // "margin-left": "80px"});
    // window.onresize = function(){
    //     location.reload();
    //  }
    if (window.matchMedia('(max-width: 767px)').matches) {
        $(".about-page-select p").each(function(){
        var $this = $(this);
        if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.remove();
    });
    }
    // $("span").each(function(){
    //     if($(this).text().toLowerCase()=="laser hair removal areas" || $(this).text().toLowerCase()=="laser hair removal prices"){
    //         $(this).parent().next().css({"width":"80%","margin":"0 auto"});
    //     }
    // });

        $(".about-page-select .container-img-text:first-child h2:first-child").replaceWith("<h1 style='"+$(".about-page-select .container-img-text:first-child h2:first-child").attr("style")+"'>"+$(".about-page-select .container-img-text:first-child h2:first-child").html()+"</h1>");
});
</script>

@endsection
