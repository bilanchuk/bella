@extends('layouts.master')
@section('title', $page->title)
@section('meta_keyword', $page->meta_keywords )
@section('meta_description', $page->meta_description)

@section('content')

@include('components.bread')

    <div class="about-page-area">
        <div class="about__us_page_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6 col-xs-12">
                        <div class="banner_h2__left_image">
                            <img alt="" src="{{Voyager::image($about->img1)}}">
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-6  col-xs-12">
                        <div class="banner_h2_Right_text">
                            <div class="wpb_wrapper">
                                <h1 style="font-size: 30px;font-weight: 400;color: rgb(195, 110, 153);
                                font-family: 'PT Serif', serif;">{!! nl2br(e($about->title)) !!}</h1>
                                <p>{!! nl2br(e($about->text1)) !!}</p>
                                <p class="">
                                    <a href="{{url($langSlug."/".App\Page::where('slug','/services')->value('slugdisplay'))}}"> {{trans('transl.Our Procedures')}} </a>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-6  col-xs-12" style="margin-top: 70px;">
                        <div class="banner_h2_Right_text" style="padding-left: 0px;">
                            <div class="wpb_wrapper">
                                <h3 style="font-size: 30px;font-weight: 400;color: rgb(195, 110, 153);
                                font-family: 'PT Serif', serif;">{{trans('transl.team')}}</h3>
                                <p>{!! nl2br(e($about->text2)) !!}</p>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6 col-xs-12"  style="padding-left: 0px;margin-top: 70px;">
                        <div class="banner_h2__left_image">
                            <img alt="" src="{{Voyager::image($about->img2)}}">
                        </div>
                    </div>


                </div>
               {!! $about->team !!}
            </div>
        </div>

        @include('components.newsletter')
        @include('components.appoint')
    </div>

@endsection
