<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/css-plugins-call.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bundle.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/colors.css')}}">
    <link rel="shortcut icon" href="{{Voyager::image('favicon1.png')}}" type="image/x-icon">
    <style>
        .display1{
            text-transform: uppercase;
        }
        .wrap-mail{
            height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            background-image: url({{Voyager::image("b,g.jpg")}});
        }
        .fa{
            margin-top: 20px;
            font-size: 30px;
        }
        .other{
            font-size: 44px;
            line-height: 24px;
            font-weight: 600;
            text-transform: uppercase;
            color: #2cabe1;
        }
        .yes{
            background-color: #88c136;
            border-radius: 20px;
            font-size: 16px;
line-height: 18px;
font-weight: 400;
text-transform: uppercase;
color: #ffffff;
padding: 10px 23px;

        }
        .no{
            background-color: #d9534f;
            border-radius: 20px;
            font-size: 16px;
line-height: 18px;
font-weight: 400;
text-transform: uppercase;
color: #ffffff;
padding: 10px 23px;
        }
        @media (max-width: 768px){
            .other{
                font-size: 25px;
            }
            #textareano{
                width: 100% !important;
                right: 0 !important;
            }
            #yes div img{
                height: 39px;
            }
        }
    </style>
    <title>Thanks for your feedback, we are getting better with your help!!</title>
</head>
<body>
    <div class="wrap-mail" id="no">
        <h1 class="light" style="width: 80%;
        text-align: center;
        font-size: 20px;
line-height: 25px;
font-weight: 400;
text-transform: uppercase;
color: #c36e99;
margin-bottom: 40px;">Thank you for your feedback! Your rating and suggestions help us maintain a high standard of service and ensures that it matches your needs.</h1>


    </div>





</body>
</html>



