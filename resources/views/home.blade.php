@extends('layouts.master')
@section('title', $page->title)
@section('meta_keyword', $page->meta_keywords )
@section('meta_description', $page->meta_description)

@section('content')

<!-- Slider area -->
<div class="container-fluid" style="z-index: 0; position: relative;">
    <div class="slider-area">
        <!-- slider start -->
        <div class="slider-inner" style="box-sizing: border-box;">
            <div id="mainSlider" class="nivoSlider nevo-slider">
                @foreach ($slider as $item)
                    <img src="{{Voyager::image($item->img)}}" alt="main slider" title="#htmlcaption{{$item->id}}"/>
                @endforeach
            </div>
            @foreach ($slider as $item)
                <div id="htmlcaption{{$item->id}}" class="nivo-html-caption slider-caption">
                    <div class="slider-progress"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-sm-12">
                                    <div class="slider-content slider-content-1 slider-animated-1 pull-left">
                                        @if ($langSlug!="es" && $langSlug!="ru")
                                            <p class="hp1">{{$item->text1}}</p>
                                        @endif
                                        <h2 class="hone top-slider-text">{{$item->text2}}</h2>
                                        <h2 class="htwo">{{$item->text3}}</h2>
                                        <div class="button-1 hover-btn-2">
                                            <a href="{{url($langSlug.$item->link)}}">{{trans('transl.more')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- slider end -->
    </div>
  </div>

<!-- Slider area end -->

<!-- poslistcategories -->
<div class="poslistcategories">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="pos_title_categories">
                    <h2><span>{{trans('transl.servicestitle')}}</span></h2>
                    <p>{{trans('transl.servicestext')}}</p>
                </div>
            </div>
        </div>
        <div class="row pos_content">
            @foreach ($services as $item)
                <div class="col-md-4">
                    <div class="item-listcategories">
                        <div class="list-categories">
                            <div class="box-inner">
                                <div class="thumb-category">
                                    <a href="{{url($langSlug.'/service/'.$item->slugdisplay)}}">
                                        <img src="{{Voyager::image($item->imghome)}}" alt="">
                                    </a>
                                </div>
                                <div class="desc-listcategoreis">
                                    <h3 class="name_categories"><a href="{{url($langSlug.'/service/'.$item->slugdisplay)}}">{{$item->title}}</a></h3>
                                    <p class="description-list">{{$item->subtitle}}</p>
                                    <div class="listcate_shop_now">
                                        <a href="{{url($langSlug.'/service/'.$item->slugdisplay)}}">{{trans('transl.readmore')}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
                <div class="about__us_page_area" style="width:100%;">
                    <div class="wpb_wrapper">
                        <p style="display: flex; justify-content:center;">
                            <a href="{{url($langSlug."/".App\Page::where('slug','/services')->value('slugdisplay'))}}"> {{trans('transl.moreservices')}} </a></p>
                    </div>
                </div>



        </div>
    </div>
</div>
<!-- poslistcategories end -->

<!-- cms aboutus -->
<div class="cms_aboutus">
    <img src="{{Voyager::image($about->bg)}}" alt="" class="img-responsive" style="height: 100%;" id="legs-img">
    <div class="cms-info">
        <div class="container">
            <div class="cms-desc">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col">
                        <div class="info-content">
                            <h4>{{trans('transl.about')}}</h4>
                            <h1> <span>{{trans('transl.welcome')}}</span></h1>
                            <p>{{$about->text}}</p>
                            <a href="{{url($langSlug."/".App\Page::where('slug','/about')->value('slugdisplay'))}}">{{trans('transl.readmore')}}</a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4 col">
                        <img src="{{Voyager::image($about->img)}}" alt="" class="img-responsive" style="height: 100%;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cms aboutus end -->

@include('components.newsletter')

<!-- home banner -->
<div class="home-banner" style="margin-top: 140px;">
    <div class="container-fluid">
        <div class="row" id="row-home-banner">
            <div class="col-sm-12 col-md-6 col" style="padding-left: 0px !important;">
                <div class="banner-box">
                    <a href="{{url($banner->link)}}"><img src="{{Voyager::image($banner->img)}}" alt="harosa"></a>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col" style="padding-left: 0px !important;">
                <div class="banner-box">
                    <a href="{{url($banner->link2)}}"><img src="{{Voyager::image($banner->img2)}}" alt="harosa"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- home banner end -->

@include('components.appoint')
<script type="text/javascript" data-pagespeed-no-defer>pagespeed.lazyLoadImages.overrideAttributeFunctions();</script><script type="application/ld+json">
    {
    "@context": "https://schema.org",
    "@type": "Bella Sana",
    "name": "Bella Sana � Med Spa Clinic",
    "image": "https://bellasanamedspa.com/storage/settings/November2020/4BKAiZmVIRvW5m8qG9l5.png",
    "@id": "",
    "url": "https://bellasanamedspa.com",
    "telephone": "(847) 576-0505",
    "priceRange": "$$",
    "address": {
      "@type": "PostalAddress",
      "streetAddress": "1400 Golf Rd #205, Des Plaines, IL",
      "addressLocality": "Chicago",
      "addressRegion": "Illinois",
      "postalcode": "60016" },
    "openingHoursSpecification": [{
      "@type": "OpeningHoursSpecification",
      "dayOfWeek": [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday"
      ],
      "opens": "10:00 AM",
      "closes": "6:00 PM"
    },{
      "@type": "OpeningHoursSpecification",
      "dayOfWeek": [
        "Saturday"
      ],
      "closes": ""
    }],
    "sameAs": ["https://www.facebook.com/bellasanamedspa/", "https://www.instagram.com/bella_sana_medspa/?igshid=lafiakmfal1r", "https://www.youtube.com/channel/UCeN6BXVq21afw6_ThB4fHnw"]
  }
  </script>
@endsection
