<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/css-plugins-call.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bundle.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/colors.css')}}">
    <link rel="shortcut icon" href="{{Voyager::image('favicon1.png')}}" type="image/x-icon">
    <style>
        .display1{
            text-transform: uppercase;
        }
        .wrap-mail{
            height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            background-image: url({{Voyager::image("b,g.jpg")}});
            background-repeat: no-repeat;
            background-size: cover;
        }
        .fa{
            margin-top: 20px;
            font-size: 30px;
        }
        .other{
            font-size: 44px;
            line-height: 24px;
            font-weight: 600;
            text-transform: uppercase;
            color: #c36e99;
        }
        .yes{
            background-color: #88c136;
            border-radius: 20px;
            font-size: 16px;
line-height: 18px;
font-weight: 400;
text-transform: uppercase;
color: #ffffff;
padding: 10px 23px;

        }
        .no{
            background-color: #d9534f;
            border-radius: 20px;
            font-size: 16px;
line-height: 18px;
font-weight: 400;
text-transform: uppercase;
color: #ffffff;
padding: 10px 23px;
        }
        a:hover{
            color: #c36e99;
            background-color: white;
            border: 2px solid #c36e99;
        }
        @media (max-width: 768px){
            .other{
                font-size: 25px;
            }
            #textareano{
                width: 100% !important;
                right: 0 !important;
            }
            #yes div img{
                height: 39px;
            }
        }
    </style>
    <title>Did you like our service?</title>
</head>
<body>
    <div class="wrap-mail main" style="<?php if(\Session::has('swaled_status') || $errors->any()){
        echo "display:none;";
    } else{
        echo "display: flex;";
    } ?>">
        <img src="{{Voyager::image(setting('site.logo'))}}" alt="" style="margin-bottom: 20px">
        <h1 class="other" style="margin-bottom: 40px; margin-top: 20px;">Did you like our service?</h1>
        <div>
            <img src="{{asset('storage/smile.png')}}" alt="" style="margin-right: 60px;
            margin-bottom: 28px;">
            <img src="{{asset('storage/sad.png')}}" alt="" style="margin-bottom: 28px;">
        </div>
        <div>
            <a href="" class="yes" style="margin-right: 28px;">Yes</a>
            <a href="" class="no">No</a>
        </div>
    </div>
    <div class="wrap-mail" id="no">
        <img src="{{Voyager::image(setting('site.logo'))}}" alt="" style="margin-bottom: 20px">
        <h1 class="light" style="width: 80%;
        text-align: center;
        font-size: 20px;
line-height: 25px;
font-weight: 400;
text-transform: uppercase;
color: #3a3a3a;
margin-bottom: 40px;">We are very sorry that we couldn't meet your expectations. We strive for 100% satisfaction and want to make sure we know our weaknesses. Please let us know what went wrong in the form below and help us improve. Thank you!</h1>
        <form action="/opno" method="post" class="">
            @csrf
            <textarea name="message" placeholder="Message" id="textareano" style="width: 211%;
            position: relative;
            right: 47%;
            /* margin-bottom: 40px; */
            background-color: rgba(255,255,255,0.54);
            border: 1px solid #bebebe;
            border-radius: 3px;
            min-height: 200px;"></textarea>
            <div class="contact-submit">
                <input type="hidden" name="mail" value="{{$mail}}">
                <input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit button" style="margin-left: 16%;">
            </div>
        </form>


    </div>

    <div class="wrap-mail" id="yes">
        <img src="{{Voyager::image(setting('site.logo'))}}" alt="" style="margin-bottom: 15px">
        <h1 style="width: 80%;
        text-align: center;
        font-size: 20px;
line-height: 25px;
font-weight: 400;
text-transform: uppercase;
color: #3a3a3a;
margin-bottom: 40px;">We are so happy to hear that you enjoyed our services! <br> Would you mind leaving feedback about your experience at Bella? <br> There are so many med spas in the area and your review may help others to choose us.  Thank you!</h1>
        <div style="display: flex;
        justify-content: space-around;
        width: 80%;">
            <a href="https://www.google.com/search?q=bella+sana+med+spa&rlz=1C5CHFA_enUS879US879&oq=bella+sana+med+spa&aqs=chrome..69i57j46i175i199j0i22i30.2920j0j7&sourceid=chrome&ie=UTF-8#lrd=0x880fb781666bec91:0xbdb52986358b2075,3" target="_blank" style="margin-right: 28px;"><img src="{{asset('storage/google.jpg')}}" alt=""></a>
            <a href="https://www.facebook.com/bellasanamedspa/reviews/?ref=page_internal" target="_blank" style="margin-right: 28px;"><img src="{{asset('storage/facebook.jpg')}}" alt=""></a>
            <a href="https://www.yelp.com/writeareview/biz/hcc0lBCgjQWdZzPYRnmNBA?return_url=%2Fbiz%2Fhcc0lBCgjQWdZzPYRnmNBA&source=biz_details_war_button" target="_blank"><img src="{{asset('storage/yelp.jpg')}}" alt=""></a>

        </div>


    </div>



    <script defer>
        document.addEventListener("DOMContentLoaded", function() {
            document.querySelectorAll(".wrap-mail").forEach(element => {
                    element.style.display="none";
                });
            document.querySelector(".main").style.display = "flex";
            document.querySelector(".yes").addEventListener("click",(e)=>{
                e.preventDefault();
                document.querySelectorAll(".wrap-mail").forEach(element => {
                    element.style.display="none";
                });
                document.querySelector("#yes").style.display="flex";

            });
            document.querySelector(".no").addEventListener("click",(e)=>{
                e.preventDefault();
                document.querySelectorAll(".wrap-mail").forEach(element => {
                    element.style.display="none";
                });
                document.querySelector("#no").style.display="flex";
            });

    });
    </script>
</body>
</html>



