<?php

use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\RequestController;
use App\Http\Controllers\SubscribeUserController;
use App\Http\Middleware\LocaleChange;
use App\Page;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => LocaleChange::getLocale()], function(){
    Route::get('/', [PageController::class, 'index']);
    Route::get('/'.Page::where('slug','/about')->value('slugdisplay'), [PageController::class, 'about']);
    Route::get('/'.Page::where('slug','/contact')->value('slugdisplay'), [PageController::class, 'contact']);
    Route::get('/'.Page::where('slug','/services')->value('slugdisplay'), [PageController::class, 'services']);
    Route::get('/'.Page::where('slug','/treatments')->value('slugdisplay'), [PageController::class, 'treatments']);
    Route::get('/service/{id}', [PageController::class, 'service']);
    Route::get('/treatment/{id}', [PageController::class, 'treatment']);
    Route::post('/news', [SubscribeUserController::class, 'index']);
    Route::post('/appoint', [AppointmentController::class, 'index']);
    Route::post('/appointcontact', [AppointmentController::class, 'contact']);
    Route::get('/vendor/voyager/rev',[PageController::class, 'rev']);
    Route::post('/sendrequest', [RequestController::class, 'send'])->name('requestcontrol');
    Route::get('/opinion/{id}', [PageController::class, 'opinion'])->name('opinion');
    Route::post('/opno', [RequestController::class, 'no']);

Route::fallback(function () {

    return redirect('/');

});
});


Route::get('/en', function(){
    return redirect('/');
});




Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('setlocale/{lang}', function ($lang) {

    $referer = Redirect::back()->getTargetUrl(); //URL предыдущей страницы
    $parse_url = parse_url($referer, PHP_URL_PATH); //URI предыдущей страницы

    //разбиваем на массив по разделителю
    $segments = explode('/', $parse_url);

    //Если URL (где нажали на переключение языка) содержал корректную метку языка
    if (in_array($segments[1], LocaleChange::$languages)) {

        unset($segments[1]); //удаляем метку
    }

    //Добавляем метку языка в URL (если выбран не язык по-умолчанию)
    if ($lang != LocaleChange::$mainLanguage){
        array_splice($segments, 1, 0, $lang);
    }

    // array_splice($segments, 1, 0, $lang);
    // if (($key = array_search("public", $segments)) !== false) {
    //     unset($segments[$key]);
    // }
    //формируем полный URL
    $url = Request::root().implode("/", $segments);
    //если были еще GET-параметры - добавляем их
    if(parse_url($referer, PHP_URL_QUERY)){
        $url = $url.'?'. parse_url($referer, PHP_URL_QUERY);
    }

    if(strpos($url,"/public")!==false){
      
     $url = strstr($url,"/public",true)."/".$lang;
    }




    return redirect($url); //Перенаправляем назад на ту же страницу

})->name('setlocale');
